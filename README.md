# Prebuilt Images and how to update 

## morello/mainline - 05140de41d31ce49b9a22df49f5939e89d300a65 (Based on 1.4)
   1. Git Clone the board-firmware repository and switch to the commit 05140de41d31ce49b9a22df49f5939e89d300a65 (https://git.morello-project.org/morello/board-firmware/-/tree/morello/mainline - Commit 05140de41d31ce49b9a22df49f5939e89d300a65)
   2. Copy the files under the board-firmware folder to Morello's system's microSD card partition. 
   3. In the microSD card partition, replace the C64 firmware image files (fip.bin, mcp_fw.bin, scp_fw.bin) under SOFTWARE folder with the AARCH64 firmware image images (fip.bin, mcp_fw.bin, and scp_fw.bin under SOFTWARE/AARCH64 folder)
  

## SR 1.3.2 (Based on 1.3)
   1. Git Clone the board-firmware repository and switch to the release 1.3 branch (https://git.morello-project.org/morello/board-firmware/-/tree/morello/release-1.3)
   2. Copy the files under board-firmware folder to Morello's system's microSD card partition. 
   3. In the microSD card partition, replace the firmware image files (fip.bin, mcp_fw.bin, scp_fw.bin) in the SOFTWARE folder with firmware image images from https://gitlab.arm.com/systemready/firmware-build/morello/prebuilt_images/-/tree/main/SR%201.3.2/SOFTWARE


## More information
   -  For details, please check section "5.2.2 Update firmware on microSD card" in the user guide (https://git.morello-project.org/morello/docs/-/blob/morello/mainline/user-guide.rst#id33)  

# How to get and build source code. 
   1. Check section 2 in the user guide to set up the build environment
       - https://git.morello-project.org/morello/docs/-/blob/morello/mainline/user-guide.rst#id6
   2. Check section 3 in the user guild to check out the source code and run the commands below.
       - https://git.morello-project.org/morello/docs/-/blob/morello/mainline/user-guide.rst#id10
       - For **morello/mainline - 05140de41d31ce49b9a22df49f5939e89d300a65**
          - repo init -u https://git.morello-project.org/morello/manifest.git -b morello/mainline -g bsp
          - repo sync 
          - git switch the repositories to the commit SHA below
            - SCP Firmware with SHA - 6c0f5b437b7bef12b029923a8bf3ed76c73b27dd
            - Trusted Firmware-A with SHA - 3ce2815936774fe924ec7538151b71085c2f18d9
            - EDK II with SHA - ce510c33dfc354e262ccd71619add069876d6a08
            - EDK II platforms with SHA - 41816f6e47f9626b35b3f4e152abdb0c68d67bca
            - Build Scripts with SHA - 91bf03d2707cb2d5bd470290b19e15bb7a27a33b
            - Manifest  with SHA - 8fe2f719a739475dc0b9c77066b04bbbfa2c7185  
       - For **SR 1.3.2**
          - repo init -u https://git.morello-project.org/morello/manifest.git -b morello/release-1.3 -g bsp 
          - repo sync
          - Replace the <morello_workspace>/bsp/uefi/edk2 folder with the edk2 code from the SR 1.x.x branch in https://gitlab.arm.com/systemready/firmware-build/morello/edk2/
          - Replace the <morello_workspace>/bsp/uefi/edk2/edk2-platforms folder with edk2-platforms from the SR 1.x.x branch in https://gitlab.arm.com/systemready/firmware-build/morello/edk2-platforms/
   3. Change the value of stack_morello_capabilities in build-scripts\config\bsp from 1 to 0. Note that this is a workaround for the WinPE boot.    
   4. Run "./build-scripts/build-all.sh -p soc -f none" to build the firmware images.